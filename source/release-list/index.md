---
layout: markdown_page
title: List of all GitLab releases
---

## [GitLab 8.4](https://about.gitlab.com/2016/01/22/gitlab-8-4-released) 

- Super-powered Search with Elasticsearch (EE only)
- Artifacts browser
- Improved GitHub Importer
- Fuzzy File Finder
- Code highlighting in Diffs
- Unsubscribe from Threads in Email
- Better links in other apps (Slack unfurls!)
- CAS Support
- Performance Monitoring
- Filter Commit Messages
- Emoji Picker Improvements
- Design Updates
- New CI features in API
- Join us for a live Q & A

## [GitLab 8.3](https://about.gitlab.com/2015/12/22/gitlab-8-3-released) 

- GitLab Pages (EE only)
- Auto-merge on Build Success
- Contribution Analytics (EE only)
- Merge Request References in Issues
- Issue Weight (EE only)
- Quickly create a new MR from the web editor
- Builds in Merge Requests
- Automatic References
- JIRA support in GitLab CE
- CI Improvements

## [GitLab 8.2](https://about.gitlab.com/2015/11/22/gitlab-8-2-released) 

- Git LFS
- Award Emoji
- Releases
- Global Milestones
- Repository Mirroring (EE only)
- Build Artifacts
- CI Runner Caching
- Copy to Clipboard buttons
- Search through Commit Messages
- CI Runner improvements
- CI & Shared Runners now on by default

## [GitLab 8.1](https://about.gitlab.com/2015/10/22/gitlab-8-1-released) 

- GitLab = GL + CI
- Design Updates
- Check out Merge Request
- Commit Status API
- Further improvements
- GitLab Mattermost Update

## [GitLab 8.0](https://about.gitlab.com/2015/09/22/gitlab-8-0-released) 

- Continuous Integration in GitLab
- Fresh Design
- Turbo Merges (Vroom Vroom)
- 50% less space used
- Reply by Email
- Quick open in Gmail
- Easily Upload files in GitLab
- Better HTTP Support
- Mattermost Improvements
- SSL Verification for Webhooks
- Public User Profile and Group Pages
- Notification Settings within the Projects main page

## [GitLab 7.14](https://about.gitlab.com/2015/08/22/gitlab-7-14-released) 

- Beta: support for long git push/pull over HTTPS
- Improved syntax highlighting
- Show who edited a discussion note
- Better LDAP group management (GitLab EE)
- YAML Variables (GitLab CI)
- Build Triggers API (GitLab CI)
- Application Settings interface (GitLab CI)
- Better support for skipped builds
- Bundling of Mattermost now, possibly RocketChat in the future

## [GitLab 7.13](https://about.gitlab.com/2015/07/22/gitlab-7-13-released) 

- Customizable Project Dashboard
- Comment on Side-by-Side diffs
- Improved Merge Request Approvals (GitLab EE)
- Docker support for GitLab CI
- allow_failure option for jobs (GitLab CI)
- Cancel all Builds (GitLab CI)
- Flexible Build Types (GitLab CI)
- Runners without Tags (GitLab CI)
- Better Omnibus Documentation

## [GitLab 7.12](https://about.gitlab.com/2015/06/22/gitlab-7-12-released) 

- SAML Support
- Web Hook for Comments
- Better performance for the Web Editor
- UI Update
- Merge Request Approvers (EE only)
- Git hook to check Maximum File Size (EE only)
- LDAP Group Sync improvements (EE only)
- .gitlab-ci.yml
- BETA: Secret Variables for runner (CI)

## [GitLab 7.11](https://about.gitlab.com/2015/05/22/gitlab-7-11-released) 

- Better looking sidebar
- Clean project dashboard
- Two-factor authentication
- User roles in comments
- Task lists everywhere
- Version Check
- License keys for Enterprise Edition
- True-up model for subscriptions
- Two-Factor Authentication for LDAP / Active Directory (EE-only)
- New GitLab CI Features
- Other awesome changes in GitLab CE

## [GitLab 7.10](https://about.gitlab.com/2015/04/22/gitlab-7-10-released) 

- Apt-get install GitLab
- Google Code Import
- Fork projects with CI
- Invite new people into project by email
- Quick view Changelog, License and Contribution guide
- Default Git Hooks (EE only feature)
- Audit log for Deploy keys (EE only feature)

## [GitLab 7.9](https://about.gitlab.com/2015/03/22/gitlab-7-9-released) 

- Dashboard
- Bitbucket importer
- Save web edit in new branch
- Drag and drop any file in markdown
- Emoji One
- Subscribe/Unsubscribe from issue or merge request
- Backup with git-annex files
- Blocking users is non-destructive
- Group level webhooks (EE only feature)

## [GitLab 7.8](https://about.gitlab.com/2015/02/22/gitlab-7-8-released) 

- GitLab.com integration: login with GitLab.com account and import projects from GitLab.com
- New file in Empty Repository
- Commit calendar
- Never lose unsaved comments!
- Project avatars
- Mention groups
- Select email for notifications
- Manage large files in Git with GitLab Annex (EE only feature)
- Improved JIRA integration (EE only feature)
- GitHub Enterprise Importer (EE only feature)
- GitLab CI versioning

